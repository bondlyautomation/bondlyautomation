package runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:./target/report/BSwapReport.html"},
							features = "src/test/java/features",
							glue = "stepsDefinition",
							tags = "@BSwap")

public class BSwapRunner {

}
