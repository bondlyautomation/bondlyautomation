package runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:./target/report/BSwapV2Report.html"},
				 features = "src/test/java/features",
				 glue = "stepsDefinition",
				 tags = "@BSwapV2")

public class BSwapV2Runner {

}
