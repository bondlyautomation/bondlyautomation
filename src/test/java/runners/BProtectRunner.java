package runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:./target/report/BProtectReport.html"},
							features = "src/test/java/features",
							glue = "stepsDefinition",
							tags = "@BProtect")

public class BProtectRunner {

}
