#Author: Alirio Aranguren - alirio@bondly.finance

Feature: Bondly Swap V2
	This is the Feature for Bondly Swap V2
	
	@BSwapV2 @BSwapV2Regression
	Scenario: Navigate through the Bondly Swap V2 home page
		Given I open CHROME
		And I navigate to the BSwap V2 Page
		And I see the V2 Logo
		Then I click connect wallet
		And I select Metamask wallet
		Then I set Metamask wallet
		And Wait for the wrong network modal to dissapear
		#And I test stuff

	@BswapV2 @BswapV2Regression @StakeScenario
	Scenario: Stake some Bondly Tokens
		Given I open CHROME
		And I navigate to the BSwap V2 Page
		And I see the V2 Logo
		Then I click connect wallet
		And I select Metamask wallet
		Then I set Metamask wallet
		And Wait for the wrong network modal to dissapear
		Then I go to the staking input
		And input 500 tokens to stake
		And I click the Stake button
		Then I confirm the transaction in my metamask wallet
		And I wait for the transaction in progress modal dissapears
		And check for the confirmation notification