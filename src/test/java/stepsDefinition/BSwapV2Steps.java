package stepsDefinition;

import base.BSwapV2Base;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import utils.BrowserConfig;

public class BSwapV2Steps {
	
	@And("I navigate to the BSwap V2 Page")
	public void goToBSwapV2() {
		BSwapV2Base base = new BSwapV2Base(BrowserConfig.getDriver());
		base.gotoBSwapV2();
	}
	
	@Then("I see the V2 Logo")
	public void getLandingPageLogo() {
		BSwapV2Base base = new BSwapV2Base(BrowserConfig.getDriver());
		base.getLandingPage();
	}
	
	@Then ("I click connect wallet")
	public void getUpperRightConnect() {
		BSwapV2Base base = new BSwapV2Base(BrowserConfig.getDriver());
		base.upperRightConnect();
	}
	
	@And ("I select Metamask wallet")
	public void selectUpperDropdownMetamask() {
		BSwapV2Base base = new BSwapV2Base(BrowserConfig.getDriver());
		base.upperRightDropdownMetamask();
	}
	
	@And ("Wait for the wrong network modal to dissapear")
	public void waitForWrongNetworkModalDissmiss() {
		BSwapV2Base base = new BSwapV2Base(BrowserConfig.getDriver());
		base.waitForWrongNetworkModalToDissapear();
	}
}
