package stepsDefinition;

import base.BProtectBase;
import io.cucumber.java.en.Then;
import utils.BrowserConfig;

public class BProtectSteps {
	
	@Then("I navigate to the BProtect Page")
	public void goToBProtect() {
		BProtectBase base = new BProtectBase(BrowserConfig.getDriver());
		base.gotoBProtect();
	}

}
