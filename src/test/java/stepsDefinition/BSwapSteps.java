package stepsDefinition;

import base.BSwapBase;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import utils.BrowserConfig;

public class BSwapSteps {
	
	@And("I navigate to the BSwap Page")
	public void goToBSwap() {
		BSwapBase base = new BSwapBase(BrowserConfig.getDriver());
		base.gotoBSwap();
	}
	
	@Then("I see the Logo")
	public void getLandingPageLogo() {
		BSwapBase base = new BSwapBase(BrowserConfig.getDriver());
		base.getLandingPage();
	}
	

}
