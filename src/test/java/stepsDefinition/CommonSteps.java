package stepsDefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

//import base.BSwapBase;
import base.BSwapV2Base;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import utils.BrowserConfig;

public class CommonSteps {

	@Given("I open EDGE")
	public void open_Edge() {
		BrowserConfig.getEdgeDriver();
	}
	
	@Given("I open CHROME")
	public void open_Chrome() {
		BrowserConfig.getChromeDriver();
	}

	
	@Then ("I set Metamask wallet")
	public void switch_to_metamask_tab() {
		BSwapV2Base base = new BSwapV2Base(BrowserConfig.getDriver());
		base.setMetamaskWallet();
	}
	
	@After
	public void tearDown(Scenario scenario) {
		final byte[] screenshot = ((TakesScreenshot) BrowserConfig.getDriver()).getScreenshotAs(OutputType.BYTES);
		scenario.attach(screenshot, "image/png", "myScreenshot");
		//BrowserConfig.getDriver().quit();
	}
	
	
	
}
