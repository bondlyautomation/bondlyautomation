package utils;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserConfig {
	private static WebDriver driver;
	
	public static WebDriver getChromeDriver() {
		//Driver Start
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/Drivers/chromedriver.exe");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addExtensions(new File("./src/main/resources/Extensions/extension_9_6_1_0.crx"));
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		System.out.println("\n\nLoaded metamask extension \n\n");
		driver = new ChromeDriver(chromeOptions);
		driver.manage().window().maximize();
		return driver;
	}
	
	public static WebDriver getIEDriver() {
		//Driver Start
		System.setProperty("webdriver.ie.driver", "./src/main/resources/Drivers/IEDriverServer.exe");
		driver = new InternetExplorerDriver();
		return driver;
	}
	
	public static WebDriver getEdgeDriver() {
		//Driver Start
		System.setProperty("webdriver.edge.driver", "./src/main/resources/Drivers/msedgedriver.exe");
		driver = new EdgeDriver();
		return driver;
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
}
