package base;

import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BondlyBSwapV2PO;
import pageObjects.MetamaskTabPO;

public class BSwapV2Base {
	
	private WebDriver driver;
	private Set<String> allTabs;
	
	public BSwapV2Base(WebDriver driver) {
		this.driver = driver;
	}
	
	public Set<String> getAllTabs() {
		return allTabs;
	}

	public void setAllTabs(Set<String> allTabs) {
		this.allTabs = allTabs;
	}
	
	public void gotoBSwapV2() {
		System.out.println("Opening BSwap V2...");
		//driver.get("https://bswap-frontend.netlify.app/");
		driver.get("https://bswap-frontend-v2-launchpad.vercel.app/stake_earn/pools");
		setAllTabs(driver.getWindowHandles());
	}
	
	public void upperRightConnect() {
		BondlyBSwapV2PO bswap = new BondlyBSwapV2PO(driver);
		bswap.getUpperRightConnect().click();
	}
	public void upperRightDropdownMetamask() {
		BondlyBSwapV2PO bswap = new BondlyBSwapV2PO(driver);
		bswap.getMetamaskDropdownButton().click();
	}
	
	public void setMetamaskWallet() {
		setAllTabs(driver.getWindowHandles());
		MetamaskTabPO metamaskObject = new MetamaskTabPO(driver);
		
		ArrayList<String> tabs = new ArrayList<String>(allTabs);;
		driver.switchTo().window(tabs.get(0));
		//new Actions(driver).sendKeys(driver.findElement(By.tagName("html")), Keys.CONTROL).sendKeys(driver.findElement(By.tagName("html")),Keys.NUMPAD3).build().perform();
		System.out.println("moved to " + driver.getTitle());
		metamaskObject.getStartedButton().click();
		metamaskObject.getImportWalletButton().click();
		metamaskObject.getIAgreeButton().click();
		metamaskObject.getSecretRecoveryPhraseInput().sendKeys("bike smile cargo oven mail gauge oppose join ensure visit stereo dilemma");
		metamaskObject.getNewPasswordInput().sendKeys("Pepperoni87");
		metamaskObject.getConfirmPasswordInput().sendKeys("Pepperoni87");
		metamaskObject.getReadAndAgreeCheckbox().click();
		metamaskObject.getImportButton().click();
		metamaskObject.getAllDoneButton().click();
		metamaskObject.getNextButton().click();
		metamaskObject.getConnectAccountButton().click();
		metamaskObject.getCloseWhatsNewModal().click();
		metamaskObject.getNetworkDropdown().click();
		metamaskObject.getRinkebyOptionDropdown().click();
		driver.switchTo().window(tabs.get(1));
		//new Actions(driver).sendKeys(driver.findElement(By.tagName("html")), Keys.CONTROL).sendKeys(driver.findElement(By.tagName("html")),Keys.NUMPAD1).build().perform();
	}
	
	public void getLandingPage() {
		BondlyBSwapV2PO bswap = new BondlyBSwapV2PO(driver);
		WebElement logo = bswap.getHomepage();
		//System.out.println(logo);
		System.out.println("Entered Site");
	}
	
	public void waitForWrongNetworkModalToDissapear() {
		BondlyBSwapV2PO bswap = new BondlyBSwapV2PO(driver);
		
		new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOf(bswap.getWrongNetworkModal()));
	}
}
