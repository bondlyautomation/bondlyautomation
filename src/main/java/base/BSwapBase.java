package base;

import java.util.ArrayList;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BondlySwapPO;
import pageObjects.MetamaskTabPO;

//import pageObjects.BondlyProtectPO;

public class BSwapBase {
	
	private WebDriver driver;
	private Set<String> allTabs;
	
	public BSwapBase(WebDriver driver) {
		this.driver = driver;
	}
	
	public Set<String> getAllTabs() {
		return allTabs;
	}

	public void setAllTabs(Set<String> allTabs) {
		this.allTabs = allTabs;
	}
	
	public void gotoBSwap() {
		System.out.println("Opening BSwap...");
		//driver.get("https://bswap-frontend.netlify.app/");
		driver.get("https://stage-market.bswap.app/");
		setAllTabs(driver.getWindowHandles());
	}
	
	public void setMetamaskWallet() {
		setAllTabs(driver.getWindowHandles());
		MetamaskTabPO metamaskObject = new MetamaskTabPO(driver);
		
		ArrayList<String> tabs = new ArrayList<String>(allTabs);;
		driver.switchTo().window(tabs.get(0));
		System.out.println("moved to " + driver.getTitle());
		metamaskObject.getStartedButton().click();
		metamaskObject.getImportWalletButton().click();
		driver.switchTo().window(tabs.get(1));
	}
	
	/*public void clickGetMetamaskGetStarted() {
		MetamaskTabPO metamaskObject = new MetamaskTabPO(driver);
		metamaskObject.getStartedButton().click();
	}*/
	
	/*public void switchToBswapTab() {
		driver.switchTo().window(tabs.get(0));
	}*/
	
	public void getLandingPage() {
		BondlySwapPO bswap = new BondlySwapPO(driver);
		//WebElement logo = bswap.getHomepage();
		//System.out.println(logo);
		System.out.println("Entered Site");
	}

	

}
