package base;

import org.openqa.selenium.WebDriver;
import pageObjects.BondlyProtectPO;

public class BProtectBase {
	private WebDriver driver;
	
	public BProtectBase(WebDriver driver) {
		this.driver = driver;
	}
	
	public void gotoBProtect() {
		BondlyProtectPO bondly = new BondlyProtectPO(driver);
		System.out.println("Opening BProtect...");
		driver.get("http://127.0.0.1:4200/");
		bondly.getHomepage();
		
	}

}
