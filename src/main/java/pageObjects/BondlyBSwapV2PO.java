package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BondlyBSwapV2PO {
	private WebDriver driver;
	
	public BondlyBSwapV2PO(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getHomepage() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		By logo = By.className("sc-bdvvtL");
		WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(logo));
		
		return elem;	
	}
	
	public WebElement getUpperRightConnect() {
		WebElement elem = driver.findElement(By.cssSelector("#root > div.sc-iJKOTD.kZjqgR > div.sc-hBUSln.giWjhp > div.sc-ksdxgE.cHmcSg > div > div"));
		return elem;
	}
	
	public WebElement getMetamaskDropdownButton() {
		WebElement elem = driver.findElement(By.cssSelector("#root > div.sc-iJKOTD.kZjqgR > div.sc-hBUSln.giWjhp > div.sc-bqiRlB.jwgGGT > div > div:nth-child(1)"));
		return elem;
	}
	
	public WebElement getWrongNetworkModal() {
		WebElement elem = driver.findElement(By.cssSelector("#page-content > div > div.sc-kHxTfl.eowmPr > div"));
		return elem;
	}

}
