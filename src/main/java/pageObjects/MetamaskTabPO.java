package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MetamaskTabPO {
	private WebDriver driver;
	
	public MetamaskTabPO (WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getStartedButton() {
		WebElement elem = driver.findElement(By.className("button"));
		return elem;
	}
	
	public WebElement getImportWalletButton() {
		WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.main-container-wrapper > div > div > div.select-action__wrapper > div > div.select-action__select-buttons > div:nth-child(1) > button"));
		return elem;
	}
	
	public WebElement getIAgreeButton() {
		WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.main-container-wrapper > div > div > div > div.metametrics-opt-in__footer > div.page-container__footer > footer > button.button.btn-primary.page-container__footer-button"));
		return elem;
	}
	
	public WebElement getSecretRecoveryPhraseInput() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		By seedPhraseInput = By.className("MuiInputBase-input");
		WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(seedPhraseInput));
		return elem;
	}
	
	public WebElement getNewPasswordInput() {
		WebElement elem = driver.findElement(By.cssSelector("#password"));
		return elem;
	}
	
	public WebElement getConfirmPasswordInput() {
		WebElement elem = driver.findElement(By.cssSelector("#confirm-password"));
		return elem;
	}
	
	public WebElement getReadAndAgreeCheckbox() {
		WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.main-container-wrapper > div > div > form > div.first-time-flow__checkbox-container > div"));
		return elem;
	}
	
	public WebElement getImportButton() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		By importButton = By.cssSelector("#app-content > div > div.main-container-wrapper > div > div > form > button");
		WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(importButton));
		return elem;
	}
	
	public WebElement getAllDoneButton() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		By allDoneButton = By.cssSelector("#app-content > div > div.main-container-wrapper > div > div > button");
		WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(allDoneButton));
		return elem;
	}
	
	public WebElement getCloseWhatsNewModal() {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		By closeModal = By.cssSelector("#popover-content > div > div > section > header > div > button");
		WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(closeModal));
		return elem;
	}
	
	public WebElement getNextButton() {
		WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.main-container-wrapper > div > div.permissions-connect-choose-account > div.permissions-connect-choose-account__footer-container > div.permissions-connect-choose-account__bottom-buttons > button.button.btn-primary"));
		return elem;
	}
	
	public WebElement getConnectAccountButton() {
		WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.main-container-wrapper > div > div.page-container.permission-approval-container > div.permission-approval-container__footers > div.page-container__footer > footer > button.button.btn-primary.page-container__footer-button"));
		return elem;
	}
	
	public WebElement getNetworkDropdown() {
		WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.app-header.app-header--back-drop > div > div.app-header__account-menu-container > div.app-header__network-component-wrapper > div"));
		return elem;
	}
	
	public WebElement getRinkebyOptionDropdown() {
		WebDriverWait wait = new WebDriverWait(driver, 100);
		By rinkeby = By.cssSelector("#app-content > div > div.menu-droppo-container.network-droppo > div > li:nth-child(6)");
		//WebElement elem = driver.findElement(By.cssSelector("#app-content > div > div.menu-droppo-container.network-droppo > div > li:nth-child(6)"));
		WebElement elem = wait.until(ExpectedConditions.presenceOfElementLocated(rinkeby));
		return elem;
	}
	
	//public WebElement
}
