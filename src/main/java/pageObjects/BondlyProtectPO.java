package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BondlyProtectPO {
	
	private WebDriver driver;
	
	public BondlyProtectPO(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getHomepage() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		By banner = By.className("details");
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(banner));
		
		return element;	
	}

}
