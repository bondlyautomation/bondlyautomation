package pageObjects;

import org.openqa.selenium.By;
//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BondlySwapPO {
	private WebDriver driver;
		
	public BondlySwapPO(WebDriver driver) {
		this.driver = driver;
	}
	
	public WebElement getHomepage() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		By logo = By.className("m-logo__img");
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(logo));
		
		return element;	
	}
	
}
